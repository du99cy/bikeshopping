package com.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.models.LoaiXe;

@Repository
public interface LoaiXeRepo extends JpaRepository<LoaiXe, String> {
	@Query(value = "{call usp_get_loai_xe}",nativeQuery = true)
	public List<LoaiXe> getAll();
}
