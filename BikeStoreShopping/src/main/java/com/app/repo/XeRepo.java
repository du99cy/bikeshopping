package com.app.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.models.Xe;

@Repository
public interface XeRepo extends JpaRepository<Xe, String> {

	@Query(value = "{call usp_get_best_sell_product}", nativeQuery = true)
	public List<Xe> list_xe_best_sell();

	@Query(value = "{call find_xe(:name,:brand_id,:category_id, :model,:price_min, :price_max, :page, :numberPerPage,:is_new)}", nativeQuery = true)
	public List<Xe> list_xe(@Param("name") String name, @Param("brand_id") String brand_id,
			@Param("category_id") String category_id, @Param("model") int model, @Param("price_min") double price_min,
			@Param("price_max") double price_max, @Param("page") int page, @Param("numberPerPage") int numberPerPage,
			@Param("is_new") boolean is_new);
	
	@Query(value = "{call usp_get_model}",nativeQuery = true)
	public List<Integer> getModel();
	
	@Query(value = "{call find_quantity_product_via_condition(:name,:brand_id,:category_id, :model,:price_min, :price_max)}",nativeQuery = true)
	public Integer getQuantityXe(@Param("name") String name, @Param("brand_id") String brand_id,
			@Param("category_id") String category_id, @Param("model") int model, @Param("price_min") double price_min,
			@Param("price_max") double price_max);
	
	@Query(value = "{call usp_get_so_san_pham_kho(:id_product)}",nativeQuery = true)
	public Integer get_quantity_from_stock(@Param("id_product") String id_product);
	
	@Query(value = "{call find_xe_via_id(:id_product)}",nativeQuery = true)
	public Optional<Xe> get_xe_via_id(@Param("id_product") String id_product);
	
}
