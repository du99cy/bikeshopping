package com.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.models.DanhGia;

@Repository
public interface DanhGiaRepo extends JpaRepository<DanhGia, Integer> {
	
	@Query(value = "{call usp_get_danh_gia(:id)}",nativeQuery = true)
	public List<DanhGia> get_list_danh_gia(@Param("id") String id);
}
