package com.app.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.models.AppUser;

@Repository
public interface StudentRepository extends JpaRepository<AppUser, Long> {
		
		Optional<AppUser> findByEmail(String email);
		
		  @Transactional
		    @Modifying
		    @Query("UPDATE AppUser a " +
		            "SET a.enabled = TRUE WHERE a.email = ?1")
		    int enableAppUser(String email);
}
