package com.app.repo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.models.GioHang;


@Repository
public interface GioHangRepo extends JpaRepository<GioHang,Long> {
	
	
	@Query(value = "{call insert_gio_hang_JAVA(:id_khach_hang,:id_xe,:so_luong)}",nativeQuery = true)
	public GioHang insertDataGioHang(@Param("id_khach_hang") String id_khach_hang,@Param("id_xe")String id_xe,@Param("so_luong")Integer so_luong);
	
	@Query(value = "{call udf_proc_delete_gio_hang(:id_khach_hang,:id_xe)}",nativeQuery = true)
	public String delete_gio_hang(@Param("id_khach_hang")String id_khach_hang,@Param("id_xe")String id_xe);
	
	@Query(value = "{call udf_get_gio_hang_by_id_khach_hang(:id_khach_hang)}",nativeQuery = true)
	public List<GioHang> get_list_gio_hang_via_id_khach_hang(@Param("id_khach_hang")  String id_khach_hang);
	
	@Query(value = "{call insert_dh(:id_khach_hang,:ngay_dat_hang,:tien_ship,:dia_chi_giao_hang,:discount,:tong_tien,:token)}",nativeQuery = true)
	public String insertDataDH(@Param("id_khach_hang") String id_khach_hang,@Param("ngay_dat_hang")LocalDateTime ngay_dat_hang,
			@Param("tien_ship")int tien_ship,@Param("dia_chi_giao_hang")String dia_chi_giao_hang,@Param("discount")int discount,
			@Param("tong_tien")float tong_tien,@Param("token")String token
			);
	@Query(value= "{call usp_get_id_don_dh_via_token(:token)}",nativeQuery = true)
	public String get_id_dh(@Param("token")String token);
	
	@Query(value = "{call insert_ctddh_java(:id_don_dat_hang,:id_muc,:id_xe,:so_luong,:gia_ban,:giam_gia)}",nativeQuery = true)
	public String insertDataIntoChiTietDonHang(@Param("id_don_dat_hang") String id_don_dat_hang,@Param("id_muc")int id_muc,
			@Param("id_xe") String id_xe,@Param("so_luong")int so_luong,@Param("gia_ban")double gia_ban,@Param("giam_gia") double giam_gia
			);
	
	@Query(value = "{call usp_delete_from_gio_hang_java(:id_khach_hang)}",nativeQuery = true)
	public String deleteFromCartDB(@Param("id_khach_hang")String id_khach_hang);
}
