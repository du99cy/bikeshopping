package com.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.models.HangXe;

@Repository
public interface HangXeRepo extends JpaRepository<HangXe, String> {
	@Query(value="{call usp_get_hang_xe}",nativeQuery = true)
	public List<HangXe> getHangXe();
}
