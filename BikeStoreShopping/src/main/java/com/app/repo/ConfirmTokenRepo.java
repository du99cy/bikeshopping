package com.app.repo;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.models.ConfirmToken;

@Repository
public interface ConfirmTokenRepo extends JpaRepository<ConfirmToken,Long>{
	@Query(value = "{call udf_create_token(:token,:created_at,:expired_at,:user_id)}",nativeQuery = true)
	public Integer saveToken(@Param("token") String token,@Param("created_at") LocalDateTime created_at,@Param("expired_at")LocalDateTime expired_at,@Param("user_id")String user_id);
	
}
