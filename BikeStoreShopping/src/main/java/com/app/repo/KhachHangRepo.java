package com.app.repo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.models.AppUser;
import com.app.models.GioHang;

import com.app.models.KhachHang;

@Repository
public interface KhachHangRepo extends JpaRepository<KhachHang, String> {
	@Query(value="{call insert_customer_java(:ten_khach_hang,:SDT,:dia_chi,:email,:password,:username) }",nativeQuery = true)
	public String postDataKhachHang(@Param("ten_khach_hang")String full_name,@Param("SDT") String phone,@Param("dia_chi")String address,@Param("email")String email,@Param("password")String password,@Param("username")String user_name);
	
	@Query(value="{call usp_get_customer_java (:email,:password)}",nativeQuery = true)
	public KhachHang getDataKhachHang(@Param("email")String email,@Param("password")String password);
	
	@Query(value="{call udf_find_user_via_email(:email)}",nativeQuery = true)
	public Integer findUserViaEmail(@Param("email")String email);
	
	@Query(value="{call udf_find_user_via_email_user_id(:email)}",nativeQuery = true)
	public String findUserViaEmailUserId(@Param("email")String email);
	
	@Query(value="{call udf_confirm_date_token(:token,:date_of_confirm)}",nativeQuery = true)
	public String confirmTokenAndValidUser(@Param("token")String token,@Param("date_of_confirm")LocalDateTime date_confirm);
	
	@Query(value = "{call get_discount_from_code(:id)}",nativeQuery = true)
	public Integer getDiscountFormCode(@Param("id")Integer id);
	

}
