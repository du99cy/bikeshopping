package com.app.models;

public enum AppUserRoles {
	MEMBER,
	ADMIN
}
