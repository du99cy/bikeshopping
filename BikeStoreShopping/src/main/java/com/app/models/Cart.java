package com.app.models;

public class Cart {
	
	private Xe xe;
	private int quantity;
	
	public Cart() {}

	public Cart(Xe xe, int quantity) {
		super();
		this.xe = xe;
		this.quantity = quantity;
	}

	public Xe getXe() {
		return xe;
	}

	public void setXe(Xe xe) {
		this.xe = xe;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Cart [xe=" + xe + ", quantity=" + quantity + "]";
	}
	
}
