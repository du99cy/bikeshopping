package com.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gio_hang",schema = "ban_hang")
public class GioHang implements Serializable {
	
	@Id
	private Long id_record;
	private String id_khach_hang;
	
	
	private String id_xe;
	private int so_luong;
	
	public GioHang() {
		
	}

	public GioHang(String id_khach_hang, String id_xe, int so_luong) {
		super();
		this.id_khach_hang = id_khach_hang;
		this.id_xe = id_xe;
		this.so_luong = so_luong;
	}

	public String getId_khach_hang() {
		return id_khach_hang;
	}

	public void setId_khach_hang(String id_khach_hang) {
		this.id_khach_hang = id_khach_hang;
	}

	public String getId_xe() {
		return id_xe;
	}

	public void setId_xe(String id_xe) {
		this.id_xe = id_xe;
	}

	public int getSo_luong() {
		return so_luong;
	}

	public void setSo_luong(int so_luong) {
		this.so_luong = so_luong;
	}

	@Override
	public String toString() {
		return "GioHang [id_khach_hang=" + id_khach_hang + ", id_xe=" + id_xe + ", so_luong=" + so_luong + "]";
	}
	

}
