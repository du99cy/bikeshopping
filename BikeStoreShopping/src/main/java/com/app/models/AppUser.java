package com.app.models;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name = "app_user")
public class AppUser implements UserDetails {
	
	
	@Id
	@GeneratedValue(
			strategy = GenerationType.IDENTITY
			
			)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "phone")
	private String phone="4234234";
	@Column(name = "address")
	private String address="adadas";
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "user_name")
	private String user_name;
	@Enumerated(EnumType.STRING)
	@Column(name = "app_user_role")
	private AppUserRoles app_user_role;
	@Column(name = "locked")
	private Boolean locked=false;
	@Column(name = "enabled")
	private Boolean enabled=false;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	
	public AppUser() {}
	public AppUser(String name,String userName, String email, String password,
			AppUserRoles appUserRole) {
		super();
		this.name = name;
		
		this.email = email;
		this.password = password;
		this.user_name = userName;
		this.app_user_role = appUserRole;
		
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority(app_user_role.name());
		return Collections.singletonList(authority);
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return user_name;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return !locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled;
	}

}
