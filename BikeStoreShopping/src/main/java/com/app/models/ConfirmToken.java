package com.app.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "confirm_token")
public class ConfirmToken {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="token")
	private String token;
	@Column(name="created_at")
	private LocalDateTime created_at;
	@Column(name="expired_at")
	private LocalDateTime expired_at;
	@Column(name="confirm_at")
	private LocalDateTime confirmed_at;
	@Column(name = "user_id")
	private String user_id;
	
	
	public ConfirmToken() {}


	public ConfirmToken(Long id, String token, LocalDateTime created_at, LocalDateTime expired_at,
			LocalDateTime confirmed_at, String user_id) {
		super();
		this.id = id;
		this.token = token;
		this.created_at = created_at;
		this.expired_at = expired_at;
		this.confirmed_at = confirmed_at;
		this.user_id = user_id;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public LocalDateTime getCreated_at() {
		return created_at;
	}


	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}


	public LocalDateTime getExpired_at() {
		return expired_at;
	}


	public void setExpired_at(LocalDateTime expired_at) {
		this.expired_at = expired_at;
	}


	public LocalDateTime getConfirmed_at() {
		return confirmed_at;
	}


	public void setConfirmed_at(LocalDateTime confirmed_at) {
		this.confirmed_at = confirmed_at;
	}


	public String getUser_id() {
		return user_id;
	}


	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	@Override
	public String toString() {
		return "ConfirmToken [id=" + id + ", token=" + token + ", created_at=" + created_at + ", expired_at="
				+ expired_at + ", confirmed_at=" + confirmed_at + ", user_id=" + user_id + "]";
	}
	
	
	
	

	
	
	
	
}
