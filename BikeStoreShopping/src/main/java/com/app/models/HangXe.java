package com.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class HangXe implements Serializable {
	@Id
	private String id_hang_xe;
	
	private String ten_hang;
	public HangXe() {}
	public HangXe(String id_hang_xe, String ten_hang) {
		super();
		this.id_hang_xe = id_hang_xe;
		this.ten_hang = ten_hang;
	}
	public String getId_hang_xe() {
		return id_hang_xe;
	}
	public void setId_hang_xe(String id_hang_xe) {
		this.id_hang_xe = id_hang_xe;
	}
	public String getTen_hang() {
		return ten_hang;
	}
	public void setTen_hang(String ten_hang) {
		this.ten_hang = ten_hang;
	}
	@Override
	public String toString() {
		return "HangXe [id_hang_xe=" + id_hang_xe + ", ten_hang=" + ten_hang + "]";
	}
	
	
}
