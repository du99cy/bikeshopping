package com.app.models;

import java.io.Serializable;

public class Filter implements Serializable {
	private String label;
	private Object child;
	
	public Filter() {
		
	}

	public Filter(String label, Object child) {
		super();
		this.label = label;
		this.child = child;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Object getChild() {
		return child;
	}

	public void setChild(Object child) {
		this.child = child;
	}

	@Override
	public String toString() {
		return "Filter [label=" + label + ", child=" + child + "]";
	}
	
}
