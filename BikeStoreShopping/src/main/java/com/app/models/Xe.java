package com.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Xe implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id_xe ;
	
    private String ten_xe ;
    private String ten_hang ;
    private String ten_loai ;
    private short model_year ;
    private double gia_ban ;
    private byte bao_hanh ;
    private String mo_ta ;
    private double khuyen_mai ;
    private String ten_nsx ;
    private Boolean is_con_hang ;
    private Boolean is_het_hang ;
    private Boolean is_khong_kinh_doanh ;
    private String anh_xe ;
    
    public Xe() {}

	public Xe(String id_xe, String ten_xe, String ten_hang, String ten_loai, short model_year, double gia_ban,
			byte bao_hanh, String mo_ta, double khuyen_mai, String ten_nsx, Boolean is_con_hang, Boolean is_het_hang,
			Boolean is_khong_kinh_doanh, String anh_xe) {
		super();
		this.id_xe = id_xe;
		this.ten_xe = ten_xe;
		this.ten_hang = ten_hang;
		this.ten_loai = ten_loai;
		this.model_year = model_year;
		this.gia_ban = gia_ban;
		this.bao_hanh = bao_hanh;
		this.mo_ta = mo_ta;
		this.khuyen_mai = khuyen_mai;
		this.ten_nsx = ten_nsx;
		this.is_con_hang = is_con_hang;
		this.is_het_hang = is_het_hang;
		this.is_khong_kinh_doanh = is_khong_kinh_doanh;
		this.anh_xe = anh_xe;
	}

	public String getId_xe() {
		return id_xe;
	}

	public void setId_xe(String id_xe) {
		this.id_xe = id_xe;
	}

	public String getTen_xe() {
		return ten_xe;
	}

	public void setTen_xe(String ten_xe) {
		this.ten_xe = ten_xe;
	}

	public String getTen_hang() {
		return ten_hang;
	}

	public void setTen_hang(String ten_hang) {
		this.ten_hang = ten_hang;
	}

	public String getTen_loai() {
		return ten_loai;
	}

	public void setTen_loai(String ten_loai) {
		this.ten_loai = ten_loai;
	}

	public short getModel_year() {
		return model_year;
	}

	public void setModel_year(short model_year) {
		this.model_year = model_year;
	}

	public double getGia_ban() {
		return gia_ban;
	}

	public void setGia_ban(double gia_ban) {
		this.gia_ban = gia_ban;
	}

	public byte getBao_hanh() {
		return bao_hanh;
	}

	public void setBao_hanh(byte bao_hanh) {
		this.bao_hanh = bao_hanh;
	}

	public String getMo_ta() {
		return mo_ta;
	}

	public void setMo_ta(String mo_ta) {
		this.mo_ta = mo_ta;
	}

	public double getKhuyen_mai() {
		return khuyen_mai;
	}

	public void setKhuyen_mai(double khuyen_mai) {
		this.khuyen_mai = khuyen_mai;
	}

	public String getTen_nsx() {
		return ten_nsx;
	}

	public void setTen_nsx(String ten_nsx) {
		this.ten_nsx = ten_nsx;
	}

	public Boolean getIs_con_hang() {
		return is_con_hang;
	}

	public void setIs_con_hang(Boolean is_con_hang) {
		this.is_con_hang = is_con_hang;
	}

	public Boolean getIs_het_hang() {
		return is_het_hang;
	}

	public void setIs_het_hang(Boolean is_het_hang) {
		this.is_het_hang = is_het_hang;
	}

	public Boolean getIs_khong_kinh_doanh() {
		return is_khong_kinh_doanh;
	}

	public void setIs_khong_kinh_doanh(Boolean is_khong_kinh_doanh) {
		this.is_khong_kinh_doanh = is_khong_kinh_doanh;
	}

	public String getAnh_xe() {
		return anh_xe;
	}

	public void setAnh_xe(String anh_xe) {
		this.anh_xe = anh_xe;
	}

	@Override
	public String toString() {
		return "Xe [id_xe=" + id_xe + ", ten_xe=" + ten_xe + ", ten_hang=" + ten_hang + ", ten_loai=" + ten_loai
				+ ", model_year=" + model_year + ", gia_ban=" + gia_ban + ", bao_hanh=" + bao_hanh + ", mo_ta=" + mo_ta
				+ ", khuyen_mai=" + khuyen_mai + ", ten_nsx=" + ten_nsx + ", is_con_hang=" + is_con_hang
				+ ", is_het_hang=" + is_het_hang + ", is_khong_kinh_doanh=" + is_khong_kinh_doanh + ", anh_xe=" + anh_xe
				+ "]";
	}
    
    
    
}
