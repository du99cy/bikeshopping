package com.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KhachHang implements Serializable {
	@Id
	private String  id_khach_hang=null;
	
	private String full_name;
	private String user_name;
	private String email;
	private String phone;
	private String address;
	private String password;
	
	public KhachHang() {}

	public KhachHang(String id_khach_hang, String full_name, String user_name, String email, String phone,
			String address, String password) {
		super();
		this.id_khach_hang = id_khach_hang;
		this.full_name = full_name;
		this.user_name = user_name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.password = password;
	}

	public String getId_khach_hang() {
		return id_khach_hang;
	}

	public void setId_khach_hang(String id_khach_hang) {
		this.id_khach_hang = id_khach_hang;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "KhachHang [id_khach_hang=" + id_khach_hang + ", full_name=" + full_name + ", user_name=" + user_name
				+ ", email=" + email + ", phone=" + phone + ", address=" + address + ", password=" + password + "]";
	}
	
}
