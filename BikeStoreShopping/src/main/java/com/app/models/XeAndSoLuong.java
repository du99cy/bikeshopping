package com.app.models;

import java.util.List;

public class XeAndSoLuong {
	private List<Xe> xe;
	private Integer so_luong;
	
	public XeAndSoLuong() {}

	public XeAndSoLuong(List<Xe> xe, Integer so_luong) {
		super();
		this.xe = xe;
		this.so_luong = so_luong;
	}

	public List<Xe> getXe() {
		return xe;
	}

	public void setXe(List<Xe> xe) {
		this.xe = xe;
	}

	public Integer getSo_luong() {
		return so_luong;
	}

	public void setSo_luong(Integer so_luong) {
		this.so_luong = so_luong;
	}

	@Override
	public String toString() {
		return "XeAndSoLuong [xe=" + xe + ", so_luong=" + so_luong + "]";
	}


	
	
}
