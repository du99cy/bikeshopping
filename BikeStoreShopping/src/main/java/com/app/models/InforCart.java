package com.app.models;

import java.util.Arrays;

public class InforCart {
	
	private String id_khach_hang;
    private String address;
    
    private Integer shipping_cost;
    private Integer discount;
    private Integer grand_total;
    private Cart[] carts;
    
    public InforCart() {}

	public InforCart(String id_khach_hang, String address, Integer shipping_cost, Integer discount, Integer grand_total,
			Cart[] carts) {
		super();
		this.id_khach_hang = id_khach_hang;
		this.address = address;
		this.shipping_cost = shipping_cost;
		this.discount = discount;
		this.grand_total = grand_total;
		this.carts = carts;
	}

	public String getId_khach_hang() {
		return id_khach_hang;
	}

	public void setId_khach_hang(String id_khach_hang) {
		this.id_khach_hang = id_khach_hang;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getShipping_cost() {
		return shipping_cost;
	}

	public void setShipping_cost(Integer shipping_cost) {
		this.shipping_cost = shipping_cost;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Integer getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(Integer grand_total) {
		this.grand_total = grand_total;
	}

	public Cart[] getCarts() {
		return carts;
	}

	public void setCarts(Cart[] carts) {
		this.carts = carts;
	}

	@Override
	public String toString() {
		return "InforCart [id_khach_hang=" + id_khach_hang + ", address=" + address + ", shipping_cost=" + shipping_cost
				+ ", discount=" + discount + ", grand_total=" + grand_total + ", carts=" + Arrays.toString(carts) + "]";
	}
    
    
}
