package com.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoaiXe implements Serializable {

	@Id
	private String id_loai_xe;
	private String ten_loai;
	
	public LoaiXe() {}

	public LoaiXe(String id_loai_xe, String ten_loai) {
		super();
		this.id_loai_xe = id_loai_xe;
		this.ten_loai = ten_loai;
	}

	public String getId_loai_xe() {
		return id_loai_xe;
	}

	public void setId_loai_xe(String id_loai_xe) {
		this.id_loai_xe = id_loai_xe;
	}

	public String getTen_loai() {
		return ten_loai;
	}

	public void setTen_loai(String ten_loai) {
		this.ten_loai = ten_loai;
	}

	@Override
	public String toString() {
		return "LoaiXe [id_loai_xe=" + id_loai_xe + ", ten_loai=" + ten_loai + "]";
	}
	
	
}
