package com.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DanhGia implements Serializable{
	@Id
	private int danh_gia_id;
	private String ten_user;
	private String id_product;
	private int rate_star;
	private String comment;
	
	public DanhGia() {}

	public DanhGia(int danh_gia_id, String id_user, String id_product, int start, String comment) {
		super();
		this.danh_gia_id = danh_gia_id;
		this.ten_user = id_user;
		this.id_product = id_product;
		this.rate_star = start;
		this.comment = comment;
	}

	public int getDanh_gia_id() {
		return danh_gia_id;
	}

	public void setDanh_gia_id(int danh_gia_id) {
		this.danh_gia_id = danh_gia_id;
	}

	public String getTen_user() {
		return ten_user;
	}

	public void setTen_user(String id_user) {
		this.ten_user = id_user;
	}

	public String getId_product() {
		return id_product;
	}

	public void setId_product(String id_product) {
		this.id_product = id_product;
	}

	public int getRate_star() {
		return rate_star;
	}

	public void setRate_star(int start) {
		this.rate_star = start;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "DanhGia [danh_gia_id=" + danh_gia_id + ", id_user=" + ten_user + ", id_product=" + id_product
				+ ", star=" + rate_star + ", comment=" + comment + "]";
	}
	
}
