package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.DanhGia;
import com.app.repo.DanhGiaRepo;

@Service
@Transactional
public class DanhGiaService {
	@Autowired
	private DanhGiaRepo danhGiaRepo;

	public DanhGiaService(DanhGiaRepo danhGiaRepo) {
		super();
		this.danhGiaRepo = danhGiaRepo;
	}
	
	public DanhGiaService() {}
	
	public List<DanhGia> get_list_danh_gia(String id)
	{
		return this.danhGiaRepo.get_list_danh_gia(id);
	}
}
