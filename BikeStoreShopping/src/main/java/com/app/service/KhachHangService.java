package com.app.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.email.EmailService;

import com.app.models.KhachHang;
import com.app.repo.ConfirmTokenRepo;
import com.app.repo.KhachHangRepo;

@Service
@Transactional
public class KhachHangService {
	@Autowired
	private KhachHangRepo khachHangRepo;
	
	@Autowired
	private ConfirmTokenRepo confirmTokenRepo;

	@Autowired
	private EmailService emailService = new EmailService();
	
	@Autowired
	private RequestTrationService requestTrationService = new RequestTrationService();
	public KhachHangService(KhachHangRepo khachHangRepo) {
		super();
		this.khachHangRepo = khachHangRepo;
	}
	
	public KhachHangService() {}
	
	public String sendDataKhachHangToDB(KhachHang kh)
	{
		Integer isEmailExist = this.khachHangRepo.findUserViaEmail(kh.getEmail());
		if (isEmailExist==1)
		{
			return "email is exist";
		}
		else
		{
			//insert to db and get id_user
			String user_id = this.khachHangRepo.postDataKhachHang(kh.getFull_name(), kh.getPhone(), kh.getAddress(), kh.getEmail(), kh.getPassword(), kh.getUser_name());
			//create token
			String token = UUID.randomUUID().toString();
			//insert token to db
			Integer success = this.confirmTokenRepo.saveToken(token, LocalDateTime.now(), LocalDateTime.now().plusMinutes(10), user_id);
			
			//authen email
			String link = "http://localhost:8081/khach_hang/confirm?token=" + token;
			
			emailService.send(kh.getEmail(), requestTrationService.buildEmail(kh.getFull_name(),  link));
			return "Go to your email to confirm registration !!!";
			
		}
			
	}
	
	public KhachHang getDataKhachHang(String email,String password)
	{
		return this.khachHangRepo.getDataKhachHang(email, password);
	}
	
	public String confirm(String token)
	{
		return this.khachHangRepo.confirmTokenAndValidUser(token, LocalDateTime.now());
	}
	public Integer getDiscountFromCode(int id)
	{
		return this.khachHangRepo.getDiscountFormCode(id);
		
	}

}
