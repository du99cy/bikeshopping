package com.app.service;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.models.AppUser;
import com.app.models.AppUserRoles;
import com.app.repo.KhachHangRepo;
import com.app.repo.StudentRepository;
import com.app.security.token.ConfirmationToken;
import com.app.security.token.ConfirmationTokenService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {
	@Autowired
	private StudentRepository userRepo;
	private static String USER_NOT_FOUND_MSG = "user with email %s not found";
	
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	@Autowired
	private ConfirmationTokenService confirmationTokenService = new ConfirmationTokenService();
	public AppUserService() {
		// TODO Auto-generated constructor stub
		super();
	}
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return this.userRepo.findByEmail(email).orElseThrow(() ->
		new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, email)));
	}
	
	public String singUpUser(AppUser appUser)
	{
		boolean userExists = this.userRepo.findByEmail(appUser.getEmail()).isPresent();
		
		if(userExists)
		{
			throw new IllegalStateException("email already taken");
		}
		String encodedPassword =bCryptPasswordEncoder.encode(appUser.getPassword());
		
		appUser.setPassword(encodedPassword);
		
		userRepo.save(appUser);
		//TODO:SEnd confirmation to token
		String token = UUID.randomUUID().toString();
		ConfirmationToken confirmationToken = new ConfirmationToken(
					token,
					LocalDateTime.now(),
					LocalDateTime.now().plusMinutes(15),
					appUser
				);
		
		confirmationTokenService.saveConfirmationToken(confirmationToken);
		
		//TODO:send email
		return token;
	}
	
	public int enableAppUser(String email) {
        return userRepo.enableAppUser(email);
    }

}
