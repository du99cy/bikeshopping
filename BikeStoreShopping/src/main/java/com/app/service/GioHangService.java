package com.app.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.GioHang;
import com.app.repo.GioHangRepo;

@Service
@Transactional
public class GioHangService {
	@Autowired
	private GioHangRepo gioHangRepo;

	public GioHangService(GioHangRepo gioHangRepo) {
		super();
		this.gioHangRepo = gioHangRepo;
	}
	
	
	  public List<GioHang> findAll() { return this.gioHangRepo.findAll(); }
	 
	
	public GioHang add(GioHang gioHang)
	{
		return this.gioHangRepo.insertDataGioHang(gioHang.getId_khach_hang(), gioHang.getId_xe(), gioHang.getSo_luong());
	}
	
	public String delete(String id_khach_hang,String id_xe)
	{
		return this.gioHangRepo.delete_gio_hang(id_khach_hang, id_xe);
	}
	
	public List<GioHang> getGioHangById_khach_hang(String id_khach_hang)
	{
		return this.gioHangRepo.get_list_gio_hang_via_id_khach_hang(id_khach_hang);
	}
	public String insertDataCart(String id_khach_hang,LocalDateTime ngay_dat_hang,int tien_ship,String dia_chi_giao_hang,int discount,float tong_tien,String token)
	{
		return this.gioHangRepo.insertDataDH(id_khach_hang, ngay_dat_hang, tien_ship, dia_chi_giao_hang, discount, tong_tien, token);
	}
	public String id_don_hang(String token)
	{
		return this.gioHangRepo.get_id_dh(token);
	}
	public String insertIntoCHiTietDH(String id_don_dat_hang,int id_muc,String id_xe,int so_luong,double gia_ban,double giam_gia) {
		return this.gioHangRepo.insertDataIntoChiTietDonHang(id_don_dat_hang, id_muc, id_xe, so_luong,gia_ban,giam_gia);
	}
	public String deleteFromCartInDB(String id_khach_hang)
	{
		return this.gioHangRepo.deleteFromCartDB(id_khach_hang);
	}
}
