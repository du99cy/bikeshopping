package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.LoaiXe;
import com.app.repo.LoaiXeRepo;

@Service
@Transactional
public class LoaiXeService  {
	@Autowired
	private LoaiXeRepo loaiXeRepo;

	public LoaiXeService(LoaiXeRepo loaiXeRepo) {
		super();
		this.loaiXeRepo = loaiXeRepo;
	}
	
	public List<LoaiXe> getALl()
	{
		return this.loaiXeRepo.getAll();
	}
}
