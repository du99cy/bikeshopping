package com.app.service;

import java.security.interfaces.XECKey;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.app.models.Xe;
import com.app.repo.XeRepo;

@Service
@Transactional
public class XeService {
	@Autowired
	private XeRepo xeRepo;

	public XeService(XeRepo xeRepo) {
		this.xeRepo = xeRepo;
	}

	public List<Xe> get_list_xe(String name, String brand_id, String category_id, int model, double price_min,
			double price_max, int page, int numberPerPage,boolean is_new) {
		return this.xeRepo.list_xe(name, brand_id, category_id, model, price_min, price_max, page, numberPerPage,is_new);
	}

	public List<Xe> get_list_xe_best_sell() {
		return this.xeRepo.list_xe_best_sell();
	}
	public List<Integer> getModel()
	{
		return this.xeRepo.getModel();
	}
	public Integer getQuantityXe(String name, String brand_id, String category_id, int model, double price_min,double price_max)
	{
		return this.xeRepo.getQuantityXe(name,brand_id,category_id,model,price_min,price_max);
	}
	public Integer get_quantity_from_stock(String id_product)
	{
		return this.xeRepo.get_quantity_from_stock(id_product);
	}
	public Optional<Xe> get_xe_via_id(String id_product)
	{
		return this.xeRepo.get_xe_via_id(id_product);
	}

}
