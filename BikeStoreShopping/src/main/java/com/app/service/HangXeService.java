package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.HangXe;
import com.app.repo.HangXeRepo;

@Service
@Transactional
public class HangXeService {
	@Autowired
	private HangXeRepo hangXeRepo;
	
	public HangXeService(HangXeRepo hangXeRepo)
	{
		this.hangXeRepo = hangXeRepo;
	}
	
	public List<HangXe> getHangXe()
	{
		return hangXeRepo.getHangXe();
	}
}
