package com.app.resource;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.DanhGiaService;

@RestController
@RequestMapping("/danh_gia")
public class DanhGia {
	@Autowired
	private DanhGiaService danhGiaService;
	
	public DanhGia()
	{
		
	}

	public DanhGia(DanhGiaService danhGiaService) {
		super();
		this.danhGiaService = danhGiaService;
	}
	@GetMapping("/all")
	public ResponseEntity<List<com.app.models.DanhGia>> getAll(@RequestParam String id)
	{
		List<com.app.models.DanhGia> li = this.danhGiaService.get_list_danh_gia(id);
		return new ResponseEntity<>(li,HttpStatus.OK);
	}
}
