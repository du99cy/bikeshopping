package com.app.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.HangXe;
import com.app.service.HangXeService;

@RestController
@RequestMapping("/hang_xe")
public class HangXeResource {

	@Autowired
	private HangXeService hangXeService;
	
	public HangXeResource(HangXeService hangXeService)
	{
		this.hangXeService = hangXeService;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<HangXe>> getAll()
	{
		List<HangXe> hang_xes = this.hangXeService.getHangXe();
		return new ResponseEntity<>(hang_xes,HttpStatus.OK);
		
	}
}
