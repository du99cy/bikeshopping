package com.app.resource;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.EmailValidator;

import com.app.models.KhachHang;
import com.app.service.KhachHangService;

@RestController
@RequestMapping("/khach_hang")
public class KhachHangResource {
	@Autowired
	private KhachHangService khachHangService;
	
	@Autowired
	private EmailValidator emailValidator;
	public KhachHangResource() {}

	public KhachHangResource(KhachHangService khachHangService) {
		super();
		this.khachHangService = khachHangService;
	}
	
	@PostMapping("/sendData")
	public ResponseEntity<HashMap<String, String>> postData(@RequestBody KhachHang kh)
	{
		HashMap<String, String> resultHash = new HashMap<String, String>();
		//check email is valid or not(this one I did in fort end)
		
		boolean isEmailValid =emailValidator.test(kh.getEmail()); 
		String result;
		if(isEmailValid)
		{
			result = this.khachHangService.sendDataKhachHangToDB(kh);
		}
		else
		{
			result = "email is invalid";
		}
		
		
		resultHash.put("result", result);
		return new ResponseEntity<>(resultHash,HttpStatus.CREATED);
	}
	
	@GetMapping("/confirm")
	public String confirmToken(@RequestParam("token") String token)
	{
		return this.khachHangService.confirm(token);
	}
	
	
	@GetMapping("/get")
	public ResponseEntity<KhachHang>getDataKhachHang(String email,String password)
	{
		KhachHang kh = this.khachHangService.getDataKhachHang(email, password);
		return new ResponseEntity<>(kh,HttpStatus.OK);
	}
	
	@GetMapping("/getDiscountFromCode")
	public Integer getDisCountFromCode(int id)
	{
		Integer discount = this.khachHangService.getDiscountFromCode(id);
		return discount;
	}

}
