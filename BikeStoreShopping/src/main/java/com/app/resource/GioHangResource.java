package com.app.resource;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.GioHang;
import com.app.models.InforCart;
import com.app.service.GioHangService;

@RestController
@RequestMapping("/gio_hang")
public class GioHangResource {
	@Autowired
	private GioHangService gioHangService;

	public GioHangResource(GioHangService gioHangService) {
		super();
		this.gioHangService = gioHangService;
	}

	@GetMapping("/all")
	public ResponseEntity<List<GioHang>> getAll() {
		List<GioHang> gio_hangs = this.gioHangService.findAll();
		return new ResponseEntity<>(gio_hangs, HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<GioHang> add(@RequestBody GioHang gh) {
		GioHang gh1 = this.gioHangService.add(gh);
		return new ResponseEntity<>(gh1, HttpStatus.CREATED);
	}

	@DeleteMapping("/delete")
	public ResponseEntity<HashMap<String, String>> delete_gio_hang(@RequestParam("id_khach_hang") String id_khach_hang,
			@RequestParam String id_xe) {
		String result = this.gioHangService.delete(id_khach_hang, id_xe);
		HashMap<String, String> message = new HashMap<String, String>();
		message.put("result", result);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@GetMapping("/getGioHangById_khach_hang")
	public ResponseEntity<List<GioHang>> getGioHangById(@RequestParam("id_khach_hang") String id_khach_hang) {
		List<GioHang> gh = this.gioHangService.getGioHangById_khach_hang(id_khach_hang);
		return new ResponseEntity<>(gh, HttpStatus.OK);
	}

	@PostMapping("/insert_data")
	public ResponseEntity<HashMap<String , String>> sendDataGioHangToDB(@RequestBody InforCart inforCart) {
		String token = UUID.randomUUID().toString();

		String result_insert = this.gioHangService.insertDataCart(inforCart.getId_khach_hang(), LocalDateTime.now(),
				inforCart.getShipping_cost(), inforCart.getAddress(), inforCart.getDiscount(),
				inforCart.getGrand_total(), token);

		String id_don_hang = this.gioHangService.id_don_hang(token);

		System.out.println(id_don_hang);

		System.out.println(result_insert);

		String result_insert_ct = "";
		for (int i = 0; i < inforCart.getCarts().length; ++i) {
			result_insert_ct = this.gioHangService.insertIntoCHiTietDH(id_don_hang, i + 1,
					inforCart.getCarts()[i].getXe().getId_xe(), inforCart.getCarts()[i].getQuantity(),
					inforCart.getCarts()[i].getXe().getGia_ban(),inforCart.getCarts()[i].getXe().getKhuyen_mai()
					);
		}
		System.out.println(result_insert_ct);
		
		//delete cart from db
		String result_delete = this.gioHangService.deleteFromCartInDB(inforCart.getId_khach_hang());
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("msg", result_delete);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
