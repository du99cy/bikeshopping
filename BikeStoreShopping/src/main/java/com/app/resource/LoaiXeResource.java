package com.app.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.LoaiXe;
import com.app.service.LoaiXeService;

@RestController
@RequestMapping("/loai_xe")
public class LoaiXeResource {
	
	@Autowired
	private LoaiXeService loaiXeService;

	public LoaiXeResource(LoaiXeService loaiXeService) {
		super();
		this.loaiXeService = loaiXeService;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<LoaiXe>>getAll()
	{
		List<LoaiXe> loai_xes = this.loaiXeService.getALl();
		return new ResponseEntity<>(loai_xes,HttpStatus.OK);
	}
}
