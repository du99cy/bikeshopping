package com.app.resource;

import java.util.HashMap;

import javax.imageio.spi.RegisterableService;

import org.aspectj.lang.annotation.RequiredTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.KhachHang;
import com.app.service.RegistrationRequest;
import com.app.service.RequestTrationService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/registration")
public class RegistrationController {
	@Autowired
	private RequestTrationService registrationService;
	@PostMapping
	public String register(@RequestBody RegistrationRequest request)
	{
		
		return registrationService.register(request);
	}
	
	@GetMapping("/hello")
	public HashMap<String, String > hello()
	{
		HashMap<String , String> a = new HashMap<String, String>();
		a.put("msg", "Hello world");
		return a;
	}
	
	@PostMapping("/testPost")
	public String add(@RequestBody String kh)
	{
		return kh;
	}
	
	@GetMapping(path = "/confirm")
    public String confirm(@RequestParam("token") String token) {
        return registrationService.confirmToken(token);
    }
}
