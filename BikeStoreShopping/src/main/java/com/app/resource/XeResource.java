package com.app.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.Filter;
import com.app.models.HangXe;
import com.app.models.LoaiXe;
import com.app.models.Xe;
import com.app.models.XeAndSoLuong;
import com.app.service.HangXeService;
import com.app.service.LoaiXeService;
import com.app.service.XeService;
import com.sun.istack.Nullable;

import net.bytebuddy.implementation.bind.annotation.Default;

@RestController
@RequestMapping("/xe")
public class XeResource {
	@Autowired
	private XeService xeService;
	@Autowired
	private HangXeService hangXeService;
	@Autowired
	private LoaiXeService loaiXeService;

	public XeResource(XeService xeService) {
		this.xeService = xeService;
	}

	@GetMapping("/all")
	public ResponseEntity<List<Xe>> get_all_xe(@RequestParam(required = false) @Nullable String name,
			@RequestParam(required = false) @Nullable String brand_id,
			@RequestParam(required = false) @Nullable String category_id,
			@RequestParam(required = false, defaultValue = "-1") int model,
			@RequestParam(required = false, defaultValue = "-1") double price_min,
			@RequestParam(required = false, defaultValue = "-1") double price_max,
			@RequestParam(required = false, defaultValue = "1") int page,
			@RequestParam(required = false, defaultValue = "32") int numberPerPage,
			@RequestParam(defaultValue = "0") boolean is_new) {

		name = (name.equals("null")) ? null : name;
		brand_id = (brand_id.equals("null")) ? null : name;
		category_id = (category_id.equals("null")) ? null : name;

		List<Xe> list_all = this.xeService.get_list_xe(name, brand_id, category_id, model, price_min, price_max, page,
				numberPerPage,is_new);
		return new ResponseEntity<>(list_all, HttpStatus.OK);
	}

	@GetMapping("/best_sell")
	public ResponseEntity<List<Xe>> get_list_xe_best_sell() {
		List<Xe> result = this.xeService.get_list_xe_best_sell();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/model")
	public ResponseEntity<List<Integer>>get_model()
	{
		List<Integer>models = this.xeService.getModel();
		return new ResponseEntity<>(models,HttpStatus.OK);
	}
	@GetMapping("/filter")
	public ResponseEntity<List<Filter>> getFilter()
	{
		List<HangXe>hang_xes =this.hangXeService.getHangXe();
		List<LoaiXe> loai_xes = this.loaiXeService.getALl();
		List<Integer> models =  this.xeService.getModel();
		
		Filter hx = new Filter("Brand",hang_xes);
		Filter lx = new Filter("Category",loai_xes);
		Filter m = new Filter("Model",models);
		
		List<Filter> filters = new ArrayList<Filter>();
		filters.add(hx);
		filters.add(m);
		filters.add(lx);
		return new ResponseEntity<>(filters,HttpStatus.OK);
	}
	@GetMapping("/product_and_so_luong")
	public ResponseEntity<XeAndSoLuong>getProductAndSoLuong(@RequestParam(required = false) @Nullable String name,
			@RequestParam(required = false)@Nullable  String brand_id,
			@RequestParam(required = false)@Nullable  String category_id,
			@RequestParam(required = false, defaultValue = "-1") int model,
			@RequestParam(required = false, defaultValue = "-1") double price_min,
			@RequestParam(required = false, defaultValue = "-1") double price_max,
			@RequestParam(required = false, defaultValue = "1") int page,
			@RequestParam(required = false, defaultValue = "32") int numberPerPage,
			@RequestParam(defaultValue = "0") boolean is_new)
	{
		name = (name.equals("null")) ? null : name;
		brand_id = (brand_id.equals("null")) ? null : brand_id;
		category_id = (category_id.equals("null")) ? null : category_id;

		List<Xe> xes = this.xeService.get_list_xe(name, brand_id, category_id, model, price_min, price_max, page,
				numberPerPage,is_new);
		Integer so_luong = this.xeService.getQuantityXe(name,brand_id,category_id,model,price_min,price_max);
		XeAndSoLuong xeAndsls = new XeAndSoLuong(xes,so_luong);
		return new ResponseEntity<>(xeAndsls,HttpStatus.OK);
	}
	
	@GetMapping("/get_quantity_from_stock")
	public ResponseEntity<Integer> get_quantity_from_stock(String id_product)
	{
		Integer quantity = this.xeService.get_quantity_from_stock(id_product);
		return new ResponseEntity<>(quantity,HttpStatus.OK);
	}
	
	@GetMapping("/get_xe_via_id")
	public ResponseEntity<Optional<Xe>> get_xe_via_id(@RequestParam("id_product")String id_product)
	{
		Optional<Xe> xe = this.xeService.get_xe_via_id(id_product);
		return new ResponseEntity<>(xe,HttpStatus.OK);
	}
	
	

}
