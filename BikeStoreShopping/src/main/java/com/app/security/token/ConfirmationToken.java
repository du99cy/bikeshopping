package com.app.security.token;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

import com.app.models.AppUser;
@Entity
@Table(name = "confirmation_token")
public class ConfirmationToken {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="token")
	private String token;
	@Column(name="created_at")
	private LocalDateTime created_at;
	@Column(name="expired_at")
	private LocalDateTime expired_at;
	@Column(name="confirmed_at")
	private LocalDateTime confirmed_at;
	
	public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	@ManyToOne
	@JoinColumn(
			nullable = false,
			name = "app_user_id"
			)
	private AppUser appUser;
	
	public ConfirmationToken() {}

	public ConfirmationToken( String token, LocalDateTime createdAt, LocalDateTime expiredAt
			,AppUser appUser) {
		super();
		
		this.token = token;
		this.created_at = createdAt;
		this.expired_at = expiredAt;
		
		this.appUser = appUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getCreatedAt() {
		return created_at;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.created_at = createdAt;
	}

	public LocalDateTime getExpiredAt() {
		return expired_at;
	}

	public void setExpiredAt(LocalDateTime expiredAt) {
		this.expired_at = expiredAt;
	}

	public LocalDateTime getConfirmedAt() {
		return confirmed_at;
	}

	public void setConfirmedAt(LocalDateTime confirmedAt) {
		this.confirmed_at = confirmedAt;
	}

	@Override
	public String toString() {
		return "ConfirmationToken [id=" + id + ", token=" + token + ", createdAt=" + created_at + ", expiredAt="
				+ expired_at + ", confirmedAt=" + confirmed_at + "]";
	}
	
	
}
