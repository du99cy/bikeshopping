package com.app.email;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.ExecutionError;

import javassist.Loader.Simple;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
public class EmailService implements EmailSender {

	private final static Logger LOGGER = LoggerFactory.getLogger(EmailService.class);
	@Autowired
	private JavaMailSender mailSender;

	@Override
	@Async
	public void send(String to, String email) {
		// TODO Auto-generated method stub
		try {

			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
			helper.setText(email, true);
			helper.setTo(to);
			helper.setSubject("Confirm your email");
			helper.setFrom("du32cy@gmail.com");
			mailSender.send(mimeMessage);

			/*
			 * SimpleMailMessage message = new SimpleMailMessage();
			 * message.setFrom("du32cy@gmail.com"); message.setTo(to); String mailSubject =
			 * "Confirm your email"; String mailContent = email;
			 * message.setSubject(mailSubject); message.setText(mailContent);
			 */
			mailSender.send(mimeMessage);

		} catch (MessagingException e) {
			LOGGER.error("failed to send email", e);
			throw new IllegalStateException("failed to send email");
		}
	}

}
